#!/bin/bash

cd /mnt/assets-prod/sharedisk/public/linked/mailtool/csv/HWN/tar2go && for file in $(find . -type f -mtime +30 -printf "%f\n");do aws s3 mv $file s3://smartzip-deliveries-prod/archive/HWN/tar2go/$file --profile backups_internal;done
