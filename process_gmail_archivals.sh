#!/bin/bash

TEMPDIR=/mnt/work/gmail
GAMPATH=/home/ec2-user/bin/gam
GYBPATH=/home/ec2-user/scripts/gmail/gyb
S3LOCATION=s3://smartzip-backup-desktop/GmailBackup

# Check the ou "TO_BE_ARCHIVED" for work to do
for ZIPPERNAME in `$GAMPATH/gam print users primaryEmail ou | grep TO_BE_ARCHIVED | cut -d"@" -f1`
        do
                # Backup the user mailbox
                echo $ZIPPERNAME
                $GYBPATH/gyb --email $ZIPPERNAME@smartzip.com --action backup --local-folder $TEMPDIR/GYB-GMail-Backup-$ZIPPERNAME --service-account
                        if [ $? -eq 0 ]
                        then
                                echo "Successfully created backup"
                        else
                                echo "Error in create process" >&2
                                exit 1
                        fi
                # Compress the backup
                cd $TEMPDIR
                tar zcf GYB-GMail-Backup-$ZIPPERNAME.gzip GYB-GMail-Backup-$ZIPPERNAME --remove-files
                        if [ $? -eq 0 ]
                        then
                                echo "Successfully compressed backup"
                        else
                                echo "Error in compress process" >&2
                                exit 1
                        fi
                # Store the backup in S3
                aws s3 mv $TEMPDIR/GYB-GMail-Backup-$ZIPPERNAME.gzip $S3LOCATION/$ZIPPERNAME/GYB-GMail-Backup-$ZIPPERNAME.gzip --profile backups_desktop
                        if [ $? -eq 0 ]
                        then
                                echo "Successfully stored backup"
                        else
                                echo "Error in store process" >&2
                                exit 1
                        fi

                # Move the user to the "TO_BE_DELETED" OU
                $GAMPATH/gam update org TO_BE_DELETED add user $ZIPPERNAME
                        if [ $? -eq 0 ]
                        then
                                echo "Successfully moved user OU"
                        else
                                echo "Error in move user OU process" >&2
                                exit 1
                        fi

                # Delete the user calendar events
                $GAMPATH/gam calendar $ZIPPERNAME wipe
                        if [ $? -eq 0 ]
                        then
                                echo "Successfully wiped user calendar events"
                        else
                                echo "Error in wipe user calendar events" >&2
                                #exit 1
                        fi
        done
